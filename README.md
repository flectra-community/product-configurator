# Flectra Community / product-configurator

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[website_product_configurator_mrp](website_product_configurator_mrp/) | 2.0.1.0.0| Website integration of MRP
[product_configurator_stock](product_configurator_stock/) | 2.0.1.0.0| Product configuration interface module for Stock
[product_configurator_restriction_policy](product_configurator_restriction_policy/) | 2.0.1.0.0| Adds a Restriction Policy for processing restrictions.
[product_configurator_sale_mrp](product_configurator_sale_mrp/) | 2.0.1.0.2| BOM Support for sales wizard
[product_configurator_mrp](product_configurator_mrp/) | 2.0.1.2.4| BOM Support for configurable products
[product_configurator_mrp_component](product_configurator_mrp_component/) | 2.0.1.1.1| BOM Support for configurable products
[product_configurator_sale](product_configurator_sale/) | 2.0.1.1.3| Product configuration interface modules for Sale
[product_configurator](product_configurator/) | 2.0.1.2.4| Base for product configuration interface modules
[website_product_configurator](website_product_configurator/) | 2.0.1.2.0| Configure products in e-shop
[product_configurator_purchase](product_configurator_purchase/) | 2.0.1.0.0| Product configuration interface for Purchase


