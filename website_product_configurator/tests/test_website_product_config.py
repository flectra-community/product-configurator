import flectra.tests


@flectra.tests.common.tagged("post_install", "-at_install")
class TestUi(flectra.tests.HttpCase):
    def test_01_admin_config_tour(self):
        self.start_tour(
            "/",
            "flectra.__DEBUG__.services['web_tour.tour'].run('config')",
            "flectra.__DEBUG__.services['web_tour.tour'].tours.config.ready",
            login="admin",
        )

    def test_02_demo_config_tour(self):
        self.start_tour(
            "/",
            "flectra.__DEBUG__.services['web_tour.tour'].run('config')",
            "flectra.__DEBUG__.services['web_tour.tour'].tours.config.ready",
            login="demo",
        )
